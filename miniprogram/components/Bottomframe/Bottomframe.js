// components/Bottomframe/Bottomframe.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    objectList:{
      type:Object,
      value:[
      {
        id:0,
        name:'总数',
        num:1
      },
      {
        id:1,
        name:'开机',
        num:1
      },
      {
        id:2,
        name:'正常',
        num:2
      },
      {
        id:3,
        name:'异常',
        num:0
      },
      {
        id:4,
        name:'点火',
        num:2
      }
    ]
  }
  },

  /**
   * 组件的初始数据
   */
  //这实在组件里面定义的我们需要从 主页面往租价里面传对象才可以
  data: {
    
  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})

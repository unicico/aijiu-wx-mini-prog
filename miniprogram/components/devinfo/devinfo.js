// components/devinfo/devinfo.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    icon: {
        type: String,
        value: "../../image/book.png"
    },
    title: {
        type: String,
        value: "设备信息"
    },
    infoArr: {
        type: Object,
        value: [
            {
                id: 1,
                title: "设备编号：",
                value: "168881000"
            }, {
                id: 2,
                title: "设备名称：",
                value: "临沂大学-图书馆二层-D4"
            }, {
                id: 3,
                title: "设备位置：",
                value: "山东-临沂-临沂大学图书馆"
            }, {
                id: 4,
                title: "经纬度信息：",
                value: "123.123456,789.123456"
            }, {
                id: 5,
                title: "设备是否损坏：",
                value: "否"
            }, {
                id: 6,
                title: "设备是否在线：",
                value: "是"
            }, {
                id: 7,
                title: "设备工作状态：",
                value: "0（空闲）"
            }, {
                id: 8,
                title: "设备注册时间：",
                value: "2022-01-01 12:24:56"
            }, {
                id: 9,
                title: "设备被扫码次数：",
                value: "15"
            }, {
                id: 10,
                title: "设备完成艾灸服务次数：",
                value: "2"
            },
        ]
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})

// componnents/sum2/sum2.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    icon: {
      type: String,
      value: "/image/book.png"
  },
  title: {
      type: String,
      value: "艾灸理疗仪 A3"
  },
  infoArr: {
      type: Object,
      value: [
          {
              id: 1,
              title: "设备编号：",
              value: "168881000"
          }, {
              id: 2,
              title: "设备状态：",
              value: "设备离线"
             
          }, {
              id: 3,
              title: "设备名称：",
              value: "临沂大学-图书馆二层-D1"
          
          }, {
              id: 4,
              title: "设备位置：",
              value: "山东-临沂-临沂大学图书馆"
          }
      ]
  }
  },
  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})

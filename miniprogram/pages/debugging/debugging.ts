var app = getApp();
var api = require("../../utils/getApiUrl")

Page({
  /**
   * 页面的初始数据
   */
  data: {
    tmp: 0,
    state: "",
    online: "",
    showColor: "",
    IMEI: "",
    jdqs: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    uid:"",
    token:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e) {
    console.log(e)
    this.setData({
      IMEI: e.IMEI,
      uid: app.globalData.userInfo.uid,
      token:app.globalData.userInfo.token
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

    //  启动计时器 - 更新设备状态
    this.data.timer = setInterval(() => {
      //  刷新设备状态
      this.refreshState()

      //  刷新寄存器状态
      this.getRelayInfo()

    }, 1200)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    clearInterval(this.data.timer)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
  },

  startDev() {
    ctlDev("startDev", this.data.uid, this.data.token, this.data.IMEI)
  },
  startFire() {
    ctlDev("startFire", this.data.uid, this.data.token, this.data.IMEI)
  },
  stopDev() {
    ctlDev("stopDev", this.data.uid, this.data.token, this.data.IMEI)
  },
  


  //  获取寄存器状态
  getRelayInfo() {
    wx.request({
      url: api.getRelayInfo(),
      method: "GET",
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: {
        uid: app.globalData.userInfo.uid,
        token: app.globalData.userInfo.token,
        IMEI: this.data.IMEI
      },
      success: (res) => {
        console.log(res.data);
        if (res.data.code === 1) {
          console.log(res.data.data);
          this.setData({
            jdqs: res.data.data.jdqs
          })
        }
      },
      fail: () => {
        wx.hideLoading()
        wx.showToast({
          title: "请求失败",
          icon: "error"
        })
      }
    })
  },

  //  刷新设备状态
  refreshState() {
    this.setData({
      tmp: app.globalData.debugging.tmp,
      state: app.globalData.debugging.state,
      online: app.globalData.debugging.online,
      showColor: app.globalData.debugging.showColor
    })
  },

  //  获取继电器的状态
  getJdqState(index: number) {
    if (index <= 6) {
      return this.data.jdqs[index - 1]
    } else {
      return this.data.jdqs[index - 2]
    }
  },

  //  获取继电器当前的相反状态
  getReverseState(state: number) {
    return state === 0 ? 1 : 0;
  },

  //  控制继电器状态
  tapSetRelay(event) {
    var index = event.currentTarget.dataset.id
    var state = this.getJdqState(index)
    var ctl = this.getReverseState(state)

    console.log("继电器", index, ", 当前: ", state, "执行", ctl);
    this.readySetRelay(index, ctl)
  },

  //  确认是否进行此操作
  readySetRelay(index: number, ctl: number) {
    var content = ctl === 1 ? `您确认要打开继电器${index}吗?` : `您确认要关闭继电器${index}吗?`
    wx.showModal({
      title: "确认操作",
      content: content,
      success: (res) => {
        if (res.confirm) {
          console.log('用户点击确定')
          wx.showLoading({title:"正在操作"})
          this.setRelay(index, ctl)
        }
      }
    })
  },

  //  设置继电器状态
  setRelay(index: number, ctl: number) {
    wx.request({
      url: api.setRelay(),
      method: "GET",
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: {
        uid: app.globalData.userInfo.uid,
        token: app.globalData.userInfo.token,
        IMEI: this.data.IMEI,
        index: index,
        ctl: ctl
      },
      fail: () => {
        wx.hideLoading()
        wx.showToast({
          title: "请求失败",
          icon: "error"
        })
      },
      success: (res) => {
        console.log(res.data);
        wx.hideLoading()
        if (res.data.code === 1) {
          console.log(res.data.data);
          wx.showToast({ title: "操作成功" })
        } else {
          wx.showToast({ title: res.data.msg, icon: "error" })
        }
      }
    })
  }
})

function ctlDev(ctl: String, uid: String, token: String, IMEI: String) {
  var url = api.stopDev()
  switch (ctl) {
    case "stopDev":
      url = api.stopDev()
      break;
    case "startDev":
      url = api.startDev()
      break;
    case "startFire":
      url = api.startFire()
      break;
    default:
      return;
  }
  wx.request({
    url: url,
    method: "GET",
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    data: {
      uid: uid,
      token: token,
      IMEI: IMEI
    },
    success: (res) => {
      let code = res.data.code
      let msg = res.data.msg
      console.log(res)
      wx.hideLoading()
      if (code == 1) {
        wx.showToast({
          title: msg
        })
      } else {
        wx.showToast({
          title: msg,
          icon: "error"
        })
      }
    },
    fail: () => {
      wx.hideLoading()
      wx.showToast({
        title: "请求失败",
        icon: "error"
      })
    }
  })
}
var api = require("../../utils/getApiUrl")
var app = getApp();
var bmap = require("./bmap-wx.min.js")
var my = require("../../utils/util")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    imei: '',
    title: '01 使用者信息',
    smalltitle: '输入您的信息以完成注册',
    name: "",
    phoneNum: "",
    adminID: "",
    longitude: 0,
    latitude: 0,
    addr: "",
    devName: "",
    province: "",
    city: "",
    district: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log("@", options.imei)
    this.setData({
      imei: options.imei
    })
    getPrivacyInfo(this);
    this.getMyPosition()
  },

  //  设备注册按钮
  regDevBtn() {
    //  校验
    if (this.data.devName == '') {
      wx.showToast({title:"请输入设备名称", icon:"error"})
      return
    }
    
    if (this.data.province == '') {
      wx.showToast({title:"请获取位置", icon:"error"})
      return
    }
    
    wx.showLoading()
    wx.request({
      url: api.devReg(),
      method: "POST",
      header: {
        "content-type": "application/json"
      },
      data: {
        uid: app.globalData.userInfo.uid,
        token: app.globalData.userInfo.token,
        imei: this.data.imei,
        name: this.data.devName,
        addr: this.data.addr,
        latitude: this.data.latitude,
        longitude: this.data.longitude,
        adminUid: this.data.adminID,
        province: this.data.province,
        city: this.data.city,
        district: this.data.district,
        reg: Date.now(),
        ownerUid: app.globalData.userInfo.uid
      },
      fail() {
        wx.hideLoading()
        wx.showToast({ title: "注册失败", icon: "none" })
      },
      success(res) {
        wx.hideLoading()
        console.log(res.data)
        let data = res.data
        if (data.code == 1) {
          wx.showToast({
            title: "注册成功", 
            complete() {
              setTimeout(() => {
                wx.navigateBack()
              }, 1200);
            }
          })
        } else {
          wx.hideLoading()
          wx.showToast({ title: "注册失败", icon: "error" })
        }
      }
    })
    console.log(this.data)
  },

  //  获取位置信息
  getMyPosition() {
    var that = this
    var BMap = new bmap.BMapWX({
      ak: 'NR4Qf2Phy0WyKuscCXMuqlYGCoTr3cK9'
    });
    BMap.regeocoding({
      fail(res){
        wx.showModal({title:res.errMsg})
      },
      success(res) {
        var data = res.originalData
        if (data.status != 0) {
          wx.showToast({
            title: "位置获取失败",
            icon: "error"
          })
          return
        }
        //  获取位置
        let addr = res.originalData.result.formatted_address

        //  获取经纬度
        data = res.originalData.result.location
        let longitude = data.lng
        let latitude = data.lat

        //  获取省市县
        data = res.originalData.result.addressComponent
        let province = data.province
        let city = data.city
        let district = data.district

        //  更新数据
        that.setData({
          longitude: longitude,
          latitude: latitude,
          province: province,
          city: city,
          district: district,
          addr: addr
        })
        console.log(res.originalData)
      }
    })
  }
})

//  获取手机号
function getPrivacyInfo(that: object) {
  wx.request({
    url: api.getUserPrivacyInfo(),
    method: "GET",
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    data: {
      uid: app.globalData.userInfo.uid,
      token: app.globalData.userInfo.token,
    },
    success(res) {
      let data = res.data
      if (data.code == 1) {
        that.setData({
          name: data.data.name,
          phoneNum: data.data.phoneNum,
          adminID: data.data.adminUid
        })
      }
    }
  })
}
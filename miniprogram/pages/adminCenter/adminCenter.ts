// pages/businesscenter/businesscenter.js
var api = require("../../utils/getApiUrl")
var util = require("../../utils/util")
var app = getApp();

Component({
  data: {
    isHaveMessage: true,
    devicesInfoList: [],
    usersInfoList: []
  },
  methods: {
    //  更新数据源
    updateSourceData() {
      //  更新管理员列表信息
      this.updateDevicesList()
      this.updateUsersList()
      this.updateIsHaveMessage()
    },

    //  更新管理员消息列表红点提示
    updateIsHaveMessage(){
      wx.request({
        url: api.adminIsHaveMessage(),
        method: "GET",
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        data: {
          uid: api.getUid(),
          token: api.getToken(),
        },
        fail(res) {
          console.log(res)
          wx.showToast({
            title: "设备刷新错误"
          })
        },
        success: (res) => {
          console.log("updateIsHaveMessage:",res);
          this.setData({
            isHaveMessage: res.data.data.res
          })
        }
      })
    },

    //  跳转到反馈界面
    goToReportList(e) {
      let filter = e.currentTarget.dataset.filter
      console.log("goToReportList: ", filter);
      wx.navigateTo({ url: `../adminReportList/adminReportList?filter=${filter}` })
    },

    //  更新管理员信息
    updateDevicesList() {
      wx.request({
        url: api.adminGetAllDevices(),
        method: "GET",
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        data: {
          uid: api.getUid(),
          token: api.getToken(),
        },
        fail(res) {
          console.log(res)
          wx.showToast({
            title: "设备刷新错误"
          })
        },
        success: (res) => {
          console.log("devicesInfoList: ", res);
          let data = res.data
          if (data.code != 1) {
            console.log("devicesInfoList: ","没有管理的用户");
            return
          }
          data = data.data
          let arr = data.devices
          console.log("data.devices: ",arr);

          //  解析状态和在线状态
          for (let index = 0; index < arr.length; index++) {
            arr[index].showState = util.getShowState(arr[index].state);
            arr[index].showOnline = util.getShowOnline(arr[index].online);
          }

          //  更新为新的值
          this.setData({
            devicesInfoList: arr
          })
        }
      })
    },

    //  更新用户信息列表
    updateUsersList() {
      wx.request({
        url: api.adminGetAllUsers(),
        method: "GET",
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        data: {
          uid: api.getUid(),
          token: api.getToken(),
        },
        fail(res) {
          console.log(res)
          wx.showToast({
            title: "设备刷新错误"
          })
        },
        success: (res) => {
          console.log(res);
          let data = res.data
          if (data.code != 1) {
            console.log("没有管理的用户");
            return
          }
          data = data.data
          let arr = data.users
          this.showUsersList(arr)
        }
      })
    },

    //  更新用户数组
    showUsersList(array: any) {
      console.log("usersInfoList:", array);
      this.setData({
        usersInfoList: array
      })
    },

    //  进入用户详情页面
    goToUserDetail(res: object) {
      //  微信设置缓存
      wx.setStorage({
        key: 'userInfo',
        data: res.currentTarget.dataset.user
      })

      //  跳转到用户详情界面
      wx.navigateTo({
        url: `../userEditPage/userEditPage`
      })
    },

    //  进入设备详情页面
    goToDevDetail(res: object) {
      console.log(res.currentTarget.dataset.device)
      wx.navigateTo({
        url: `../devEditPage/devEditPage?res=${JSON.stringify(res.currentTarget.dataset.device)}`
      })
    }
  },

  //  页面就绪时
  ready() {
    //  更新页面数据源
    this.updateSourceData()
  },
});
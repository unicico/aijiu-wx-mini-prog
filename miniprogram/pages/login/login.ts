var api = require("../../utils/getApiUrl")
var my = require("../../utils/util")
var app = getApp();

Page({
  data: {
    userImg: null,
  },

  //  当页面就绪
  onReady() {
    //  更新全局变量
    app.globalData.userAvatarUrl = wx.getStorageSync('userImg')
    app.globalData.userNickName = wx.getStorageSync('userNickName')

    this.setData({
      userImg: app.globalData.userAvatarUrl
    })
  },

  // 用户登录事件
  userLogin() {
    this.checkUserInfo(()=>{
      login(api.userLoginUrl(), "0")
    })
  },

  //  管理员登陆事件
  adminLogin() {
    this.checkUserInfo(()=>{
      wx.navigateTo({url:"../adminLogin/adminLogin"})
    })
  },
  
  //  判断是否有缓存进行登录
  checkUserInfo(callback:CallableFunction) {
    wx.showLoading()
    //  判断用户有无缓存
    if (!this.data.userImg) {
      //  询问用户信息
      wx.getUserProfile({
        desc: "获取您的信息用来展示",
        success: (res) => {
          //  解析数据
          let userImg = res.userInfo.avatarUrl
          let userNickName = res.userInfo.nickName

          //  存储数据
          app.globalData.userAvatarUrl = userImg
          app.globalData.userNickName = userNickName

          //  设置缓存
          try {
            wx.setStorageSync("userImg",userImg)
            wx.setStorageSync("userNickName",userNickName)
          } catch (error) {
            return
          }

          //  执行登录
          wx.hideLoading()
          callback()
        }
      })
      return
    }

    //  直接登录
    wx.hideLoading()
    callback()
  }
})

//  通用登录事件
function login(url: string, type: string) {
  wx.showLoading({
    title: "正在登录"
  })
  wx.login({
    success(res) {
      if (!res.code) {
        my.showError("登录失败")
        console.log('登录失败: ' + res.errMsg)
        return
      }
      console.log(res.code)
      //发起网络请求
      wx.request({
        url: url,
        method: "POST",
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        data: {
          code: res.code,
          type: type
        },
        fail() {
          my.showError()
        },
        success(res) {
          app.globalData.userInfo.loginType = Number.parseInt(type)
          handleSuccessRes(res)
          console.log(app.globalData.userInfo)
        }
      })
    }
  })
}

//  处理成功事件
function handleSuccessRes(res: WechatMiniprogram.RequestSuccessCallbackResult<string | Record<string, any> | ArrayBuffer>) {
  console.log(res.data)
  wx.hideLoading()
  if (res.data.code == 1) {
    //  更新全局变量
    app.globalData.userInfo.uid = res.data.data.uid
    app.globalData.userInfo.token = res.data.data.token
    //  跳转页面主页面
    wx.reLaunch({
      url: "../index/index"
    })
  } else if (res.data.code == -1) {
    //  跳转到注册页面
    wx.navigateTo({
      url: "../userReg/userReg"
    })
  } else { my.showError(res.data.msg) }
}
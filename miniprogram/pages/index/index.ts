var api = require("../../utils/getApiUrl")
var app = getApp();
var util = require("../../utils/util")

//  初始化轮换图片地址
const imageCdn = '../image/';

//  轮换展示列表
const swiperList = [
  {
    image: `../../image/02.jpg`,
  },
  {
    image: `../../image/03.jpg`,
  },
  {
    image: `../../image/04.jpg`,
  },
  {
    image: `../../image/05.jpg`,
  },
];

Page({
  data: {
    current: 1,
    autoplay: true,
    duration: 500,
    interval: 5000,
    swiperList,
    deviceNum: 0,
    devicesInfoList: [],
    showOnline: "",
    showState: "",
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.refreshData()
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    //  刷新所有数据
    this.refreshData()
  },

  //  跳转到我的提交信息
  goToMyReportList() {
    wx.navigateTo({ url: "../myReportList/myReportList" })
  },

  //  点击具体条目 - 跳转到具体页面
  goToDetail(event) {
    app.globalData.selectedDev = event.currentTarget.dataset.item
    console.log("goToDetail:", app.globalData.selectedDev);
    wx.navigateTo({
      url: "../devCtrl/devCtrl",
    })
  },

  //  刷新设备列表项目
  refreshData() {
    var that = this
    var info = app.globalData.userInfo
    console.log("app.globalData.userInfo: ", info);
    wx.request({
      url: api.getUserAllDevBasInfo(),
      method: "GET",
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: {
        uid: info.uid,
        token: info.token,
      },
      fail(res) {
        console.error(res)
        wx.showToast({
          title: "设备刷新错误"
        })
      },
      success(res) {
        if (res.data.data.devices != null) {
          that.setData({
            devicesInfoList: [],
            deviceNum: res.data.data.num
          })

          //  若数量为0 - 则不再维护列表
          if (res.data.data.num == 0) {
            return
          }
          console.log(res.data.data.devices);

          //  维护列表数组
          const arr = new Array
          for (let i = 0; i < res.data.data.devices.length; i++) {
            var itme = {}
            itme["name"] = res.data.data.devices[i].name
            itme["addr"] = res.data.data.devices[i].addr
            itme["online"] = res.data.data.devices[i].online
            itme["state"] = res.data.data.devices[i].state
            itme["tmp"] = res.data.data.devices[i].tmp
            itme["imei"] = res.data.data.devices[i].imei
            itme["showOnline"] = util.getShowOnline(res.data.data.devices[i].online)
            itme["showState"] = util.getShowState(res.data.data.devices[i].state)
            itme["reaminNumber"] = res.data.data.devices[i].remainNumber
            arr.push(itme)
          }
          that.setData({
            devicesInfoList: arr
          })
          app.globalData.userInfo.deviceNum = arr.length
          console.log("所有设备列表:", arr)
        }
      }
    })
  },

  //  跳转到注册页面
  goToRegDev() {
    wx.navigateTo({
      url: "../scancode/scanccode"
    })
  },

  //  跳转到设备注册
  regDev() {
    var that = this;
    //  启动扫码
    wx.scanCode({
      scanType: ['qrCode'],
      fail() {
        wx.showToast({
          title: "扫码失败",
          icon: "none",
        })

      },
      success(res) {
        console.log("扫描到码的key" + res.result);
        // console.log("输出数组"+that.data.devicesInfoList);
        var imeii = res.result;
        var target = null
        var flag = 0;
        
        for (let i = 0; i < that.data.devicesInfoList.length; i++) {
          if (imeii == that.data.devicesInfoList[i].imei) {
            // console.log("任意取值"+that.data.devicesInfoList);
            target = that.data.devicesInfoList[i]
            flag = 1;
            break;
          }
        }

        if (flag == 1) {
          app.globalData.selectedDev = target
          wx.navigateTo({
            url: "../devCtrl/devCtrl?from=code"
          })
        } else {
          wx.navigateTo({
            url: "../regDev/reg?imei=" + res.result,
          })
        }
      }

    })
  }
})
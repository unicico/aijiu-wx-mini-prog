// pages/userEditPage/userEditPage.ts
var my = require("../../utils/util.js")
var app = getApp();
var api = require("../../utils/getApiUrl")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    uid: 0,
    name: "",
    gender: 0,
    regTime: "",
    pcLastConDate: "",
    phoneNum: ""
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    var that = this
    wx.getStorage({
      key: 'userInfo',
      success: function (res) {
        console.log(res.data)
        that.setData({
          uid: res.data.uid,
          name: res.data.name,
          gender: res.data.gender,
          regTime: res.data.regTime,
          pcLastConDate: res.data.pcLastConDate,
          phoneNum: res.data.phoneNum
        })
      }
    })
  },
  delUser() {
    console.log(this.data);
    
    wx.showLoading()
    wx.request({
      url: api.adminDelUser(),
      method: "GET",
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: {
        uid: app.globalData.userInfo.uid,
        token: app.globalData.userInfo.token,
        toUid:this.data.uid,
      },
      fail(res) {
        wx.hideLoading()
        console.log(res)
        wx.showToast({
          title: "设备刷新错误"
        })
      },
      success: (res) => {
        wx.hideLoading()
        if (res.data.code === 1) {
          wx.showToast({
            title: "操作成功", complete: () => {
              setTimeout(() => {
                wx.navigateBack()
              }, 1200);
            }
          })
        } else {
          wx.showToast({ title: res.data.msg, icon: "error" })
        }
        console.log(res);
      }
    })
  },

  saveUser() {
    console.log(this.data);
    
    wx.showLoading()
    wx.request({
      url: api.adminUpdateUserInfo(),
      method: "GET",
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: {
        uid: app.globalData.userInfo.uid,
        token: app.globalData.userInfo.token,
        toUid:this.data.uid,
        newPhoneNum: this.data.phoneNum,
        newName: this.data.name
      },
      fail(res) {
        wx.hideLoading()
        console.log(res)
        wx.showToast({
          title: "设备刷新错误"
        })
      },
      success: (res) => {
        wx.hideLoading()
        if (res.data.code === 1) {
          wx.showToast({
            title: "操作成功", complete: () => {
              setTimeout(() => {
                wx.navigateBack()
              }, 1200);
            }
          })
        } else {
          wx.showToast({ title: res.data.msg, icon: "error" })
        }
        console.log(res);
      }
    })
  },
  delUserBtn() {
    console.log("del");
    wx.showModal({
      title: "确认删除",
      content: "确认要删除该用户吗?",
      success: (res) => {
        if (res.confirm) {
          console.log("Yes");
          this.delUser()
        }
      }
    })
  },

  saveUserBtn() {
    console.log("Save");
    wx.showModal({
      title: "确认修改",
      content: "确认要修改该用户信息吗?",
      success: (res) => {
        if (res.confirm) {
          console.log("Yes");
          this.saveUser()
        }
      }
    })
  }
})
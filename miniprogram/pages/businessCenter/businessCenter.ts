var api = require("../../utils/getApiUrl")
var util = require("../../utils/util")
var app = getApp();

Component({
  data: {
    isHaveMessage: true,
    devicesInfoList: [],
  },
  methods: {
    //  更新页面数据以及UI
    updateSourceData() {
      //  更新用户设备数组
      this.updateDevicesInfoList()
    },

    //  进入设备详情页面
    goToDevDetail(res: object) {
      console.log(res.currentTarget.dataset.device)
      wx.navigateTo({
        url: `../devEditPage/devEditPage?res=${JSON.stringify(res.currentTarget.dataset.device)}`
      })
    },

    //  更新用户设备数组
    updateDevicesInfoList() {
      console.log("用户界面更新设备列表数组");
      wx.request({
        url: api.getUserAllDevBasInfo(),
        method: "GET",
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        data: {
          uid: api.getUid(),
          token: api.getToken(),
        },
        fail(res) {
          console.log(res)
          wx.showToast({
            title: "设备刷新错误"
          })
        },
        success: (res) => {
          console.log("devicesInfoList: ", res);
          let data = res.data
          if (data.code != 1) {
            console.log("devicesInfoList: ", "没有管理的用户");
            return
          }
          data = data.data
          let arr = data.devices
          console.log("data.devices: ", arr);

          //  解析状态和在线状态
          for (let index = 0; index < arr.length; index++) {
            arr[index].showState = util.getShowState(arr[index].state);
            arr[index].showOnline = util.getShowOnline(arr[index].online);
          }

          //  更新为新的值
          this.setData({
            devicesInfoList: arr
          })
        }
      })
    }
  },
  //  页面就绪时
  ready() {
  },
});
// pages/devEditPage/devEditPage.ts
var bmap = require("./bmap-wx.min.js")
var my = require("../../utils/util.js")
var app = getApp();
var api = require("../../utils/getApiUrl")
var day = require('dayjs')

Page({
  /**
   * 页面的初始数据
   */
  data: {
    loginType: 0,
    longitude: 0,
    latitude: 0,
    province: "",
    city: "",
    district: "",
    addr: "",
    name: "",
    imei: "",
    ownerUid: "",
    totalNumber: 0,
    reg: "",
    adminUid: "",
    remainNumber: 0
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e: object) {
    let data = JSON.parse(e.res)
    console.log("@", data);
    if (e.res) {
      this.setData({
        longitude: data.longitude,
        latitude: data.latitude,
        province: data.province,
        city: data.city,
        district: data.district,
        addr: data.addr,
        name: data.name,
        imei: data.imei,
        ownerUid: data.ownerUid,
        totalNumber: data.totalNumber,
        reg: day(Number.parseInt(data.reg)).format('YYYY年MM月DD日 hh:mm:ss'),
        adminUid: data.adminUid,
        remainNumber: data.remainNumber,
        loginType: app.globalData.userInfo.loginType
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  //  删除设备按钮
  delDevBtn() {
    wx.showModal({
      title: "确认删除",
      content: "确认要删除该设备吗?",
      success: (res) => {
        if (res.confirm) {
          console.log("Yes");
          this.deleteDev()
        }
      }
    })
  },

  //  保存按钮
  saveDevBtn() {
    if (this.data.loginType === 0) {
      wx.showToast({title:"请联系设备管理员", icon:"error"})
      return
    }


    wx.showModal({
      title: "确认保存",
      content: "确认要修改设备信息吗?",
      success: (res) => {
        if (res.confirm) {
          this.saveInfo()
        }
      }
    })
  },

  //  删除设备信息
  deleteDev() {
    var that = this
    wx.showLoading()
    wx.request({
      url: that.data.loginType == 1 ? api.adminDeleteDev() : api.deleteDev(),
      method: "GET",
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: {
        uid: app.globalData.userInfo.uid,
        token: app.globalData.userInfo.token,
        IMEI: this.data.imei,
      },
      fail(res) {
        wx.hideLoading()
        console.log(res)
        wx.showToast({
          title: "设备刷新错误"
        })
      },
      success: (res) => {
        wx.hideLoading()
        if (res.data.code === 1) {
          wx.showToast({
            title: "操作成功", complete: () => {
              setTimeout(() => {
                wx.navigateBack()
              }, 1200);
            }
          })
        } else {
          wx.showToast({ title: res.data.msg, icon: "error" })
        }
        console.log(res);
      }
    })
  },

  //  保存存储信息
  saveInfo() {
    wx.showLoading()
    let devInfo = this.data
    console.log(this.data);

    //  准备提交的信息
    var data = {
      uid: app.globalData.userInfo.uid,
      token: app.globalData.userInfo.token,
      IMEI: devInfo.imei,

      longitude: devInfo.longitude,
      latitude: devInfo.latitude,
      province: devInfo.province,
      city: devInfo.city,
      district: devInfo.district,
      addr: devInfo.addr,
      name: devInfo.name,
      ownerUid: devInfo.ownerUid,
      totalNumber: devInfo.totalNumber,
      remainNumber: devInfo.remainNumber
    }

    wx.request({
      url: api.adminUpdateDevInfo(),
      method: "GET",
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: data,
      fail(res) {
        wx.hideLoading()
        console.log(res)
        wx.showToast({
          title: "设备刷新错误"
        })
      },
      success: (res) => {
        wx.hideLoading()
        console.log(res);
        if (res.data.code === 1) {
          wx.showToast({
            title: "操作成功", complete: () => {
              setTimeout(() => {
                wx.navigateBack()
              }, 1200);
            }
          })
        } else {
          wx.showToast({ title: res.data.msg, icon: "error" })
        }
      }
    })

    console.log(data);
  },

  //  获取位置
  getMyPosition() {
    wx.showLoading()
    var that = this
    var BMap = new bmap.BMapWX({
      ak: 'NR4Qf2Phy0WyKuscCXMuqlYGCoTr3cK9'
    });
    BMap.regeocoding({
      success(res) {
        wx.hideLoading()
        var data = res.originalData
        if (data.status != 0) {
          wx.showToast({
            title: "位置获取失败",
            icon: "error"
          })
          return
        }
        //  获取位置
        let addr = res.originalData.result.formatted_address

        //  获取经纬度
        data = res.originalData.result.location
        let longitude = data.lng
        let latitude = data.lat

        //  获取省市县
        data = res.originalData.result.addressComponent
        let province = data.province
        let city = data.city
        let district = data.district

        //  更新数据
        that.setData({
          "longitude": longitude,
          "latitude": latitude,
          "province": province,
          "city": city,
          "district": district,
          "addr": addr,
        })
        console.log(res.originalData)
      }
    })
  }
})
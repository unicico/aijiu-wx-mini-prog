// pages/myReportList/myReportList.ts

var api = require("../../utils/getApiUrl")
var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    reportNum: 0,
    reportList: [
    ]
  },

  //  查看报告详情
  goToReportDetail(e: object) {
    let selectedReportID = e.currentTarget.dataset.id;
    console.log(selectedReportID);
    wx.navigateTo({ url: "../reportDetail/reportDetail?id=" + selectedReportID })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    userGetReportList(this)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    userGetReportList(this)
  },
})

//  获取详细数据--更新UI
function userGetReportList(that: object) {
  wx.request({
    url: api.userGetReportList(),
    method: "GET",
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    data: {
      uid: app.globalData.userInfo.uid,
      token: app.globalData.userInfo.token,
    },
    success(res) {
      res = res.data
      console.log(res);

      if (res.code == 1) {
        //  过滤数据
        that.setData({
          reportList: res.data.reports,
          reportNum: res.data.reports.length
        })
      }
    }
  })
}
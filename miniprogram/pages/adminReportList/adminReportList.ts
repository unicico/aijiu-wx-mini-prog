var api = require("../../utils/getApiUrl")
var app = getApp();

Page({
  data: {
    filter: true,
    reportNum: 0,
    reportList: [
    ]
  },

  //  查看报告详情
  goToReportDetail(e: object) {
    let selectedReportID = e.currentTarget.dataset.id;
    console.log(selectedReportID);
    wx.navigateTo({ url: "../adminReportDetail/adminReportDetail?id=" + selectedReportID })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e) {
    console.log("是否过滤数据: ", e);
    this.setData({
      filter: e.filter == 'true'
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    adminGetReportList(this)
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    adminGetReportList(this)
  },
})



//  获取详细数据--更新UI
function adminGetReportList(that: object) {
  wx.request({
    url: api.adminGetReportList(),
    method: "GET",
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    data: {
      uid: app.globalData.userInfo.uid,
      token: app.globalData.userInfo.token,
    },
    success(res) {
      res = res.data
      console.log(res);

      if (res.code == 1) {
        //  过滤数据
        let resList = res.data.reports.filter((item) => {
          return that.data.filter == true ? item.state === 0 : true
        })

        that.setData({
          reportList: resList,
          reportNum: resList.length
        })
      }
    }
  })
}
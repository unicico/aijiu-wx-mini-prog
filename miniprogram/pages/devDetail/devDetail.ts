var app = getApp();
var api = require("../../utils/getApiUrl")

Component({
  data: {
    border: {
      color: '#f6f6f6',
    },
    uid: "",
    token: "",
    IMEI: "",
    name: "",
    online: "",
    tmp: "",
    addr: "",
    state: "",
    showColor: "red",
  },
  methods: {
    goToReport() {
      wx.navigateTo({ url: "../postReport/postReport?IMEI=" + this.data.IMEI + "&type=0" })
    },
    goToDebugging() {
      wx.navigateTo({ url: "../debugging/debugging?IMEI=" + this.data.IMEI })
    },
    startDev() {
      ctlDev("startDev", this.data.uid, this.data.token, this.data.IMEI)
    },
    startFire() {
      ctlDev("startFire", this.data.uid, this.data.token, this.data.IMEI)
    },
    stopDev() {
      ctlDev("stopDev", this.data.uid, this.data.token, this.data.IMEI)
    },
    getDevInfo(uid: String, token: String, IMEI: String) {
      console.log("@", uid, token, IMEI);
      wx.request({
        url: api.getDevDetail(),
        method: "GET",
        header: {
          "content-type": "application/x-www-form-urlencoded"
        },
        data: {
          uid: uid,
          token: token,
          IMEI: IMEI
        },
        success: (res) => {
          console.log(res)
          let data = res.data.data
          this.setData({
            uid: uid,
            token: token,
            IMEI: data.imei,
            name: data.name,
            online: data.online == 1 ? "在线" : "离线",
            tmp: data.tmp,
            addr: data.addr,
            state: data.state == 0 ? "空闲" : data.state == 1 ? "启动" : data.state == 2 ? "点火" : "未知",
            showColor: data.online == 0 ? "#9a9a9a" : data.state == 0 ? "#00A870" : data.state == 1 ? "#0052D9" : data.state == 2 ? "#D43030" : "black"
          }),

            app.globalData.debugging.online = this.data.online
          app.globalData.debugging.state = this.data.state
          app.globalData.debugging.showColor = this.data.showColor
          app.globalData.debugging.tmp = this.data.tmp

          console.log(app.globalData.debugging.online);
          console.log(app.globalData.debugging.state);
          console.log(app.globalData.debugging.showColor);

          console.log("@", this.data)
        }
      })
    }
  },
  ready() {
    console.log("@", app.globalData.selectedDev.imei)
    var that = this
    this.data.timer = setInterval(() => {
      this.getDevInfo(app.globalData.userInfo.uid, app.globalData.userInfo.token, app.globalData.selectedDev.imei)
    }, 1200)
  },
  detached() {
    console.log(222222222222222222);
    clearInterval(this.data.timer)
  },
});

function ctlDev(ctl: String, uid: String, token: String, IMEI: String) {
  var url = api.stopDev()
  switch (ctl) {
    case "stopDev":
      url = api.stopDev()
      break;
    case "startDev":
      url = api.startDev()
      break;
    case "startFire":
      url = api.startFire()
      break;
    default:
      return;
  }
  wx.request({
    url: url,
    method: "GET",
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    data: {
      uid: uid,
      token: token,
      IMEI: IMEI
    },
    success: (res) => {
      let code = res.data.code
      let msg = res.data.msg
      console.log(res)
      wx.hideLoading()
      if (code == 1) {
        wx.showToast({
          title: msg
        })
      } else {
        wx.showToast({
          title: msg,
          icon: "error"
        })
      }
    },
    fail: () => {
      wx.hideLoading()
      wx.showToast({
        title: "请求失败",
        icon: "error"
      })
    }
  })
}
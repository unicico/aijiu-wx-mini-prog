var api = require("../../utils/getApiUrl")
var app = getApp();
var day = require('dayjs')

Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: "",
    uid: "",
    token: "",
    adminUid: 0,
    completeDate: null,
    content: "",
    imei: null,
    name: "",
    phoneNum: "",
    postDate: "",
    state: 0,
    type: 0,
    userUid: 0,
    showCompleteDate: "",
    showPostDate: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(e) {
    console.log(e.id);
    this.setData({
      id: e.id,
      uid: app.globalData.userInfo.uid,
      token: app.globalData.userInfo.token,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    adminGetReportDetail(this)
  },
  //  确认完成按钮
  dealBtn(){
    wx.showLoading()
    wx.request({
      url: api.adminSetReport(),
      method: "GET",
      header: {
        "content-type": "application/x-www-form-urlencoded"
      },
      data: {
        uid: this.data.uid,
        token: this.data.token,
        id: this.data.id
      },
      fail(){
        wx.hideLoading()
        wx.showToast({title:"网络错误", icon:"error"})
      },
      success:(res)=>{
        wx.hideLoading()
        console.log(res);
        if (res.data.code == 1) {
          wx.showToast({title:"操作成功"})
          setTimeout(() => {
            wx.navigateBack()
          }, 1200);
        }else{
          wx.showToast({title:"操作失败", icon:"error"})
        }
      }
    })
  }
})

//  获取报告详情
function adminGetReportDetail(that: object) {
  wx.request({
    url: api.adminGetReportDetail(),
    method: "GET",
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    data: {
      uid: that.data.uid,
      token: that.data.token,
      id: that.data.id
    },
    success(res) {
      res = res.data
      console.log(res);
      if (res.code == 1) {
        let data = res.data
        that.setData({
          adminUid: data.adminUid,
          completeDate: data.completeDate,
          content: data.content,
          imei: data.imei,
          name: data.name,
          phoneNum: data.phoneNum,
          postDate: data.postDate,
          state: data.state,
          type: data.type,
          userUid: data.userUid,
          showCompleteDate: day(data.completeDate).format("YYYY-MM-DD hh:mm:ss"),
          showPostDate: day(data.postDate).format("YYYY-MM-DD hh:mm:ss"),
        })
      }
    }
  })
}
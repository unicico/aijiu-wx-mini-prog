var api = require("../../utils/getApiUrl")
var app = getApp();
var day = require('dayjs')

Page({
  data: {
    id: "",
    imei: "",
    postDate: Date.now(),
    adminUid: 0,
    userUid: 0,
    name: "",
    phoneNum: "",
    content: "",
    uid: "",
    token: "",
    type: "",
    showPostDate: ""
  },

  /**
   * 生命周期函数--监听页面加载--更新页面变量
   */
  onLoad(e) {
    this.setData({
      imei: e.IMEI,
      userUid: app.globalData.userInfo.uid,
      uid: app.globalData.userInfo.uid,
      token: app.globalData.userInfo.token,
      type: e.type,
      showPostDate: day(this.data.postDate).format('YYYY年MM月DD日 hh:mm:ss')
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    getPrivacyInfo(this)
    this.setData({
      id: genReportID()
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
  },
  
  //  提交报告
  postRptBtn() {
    if (this.data.content == "") {
      wx.showToast({title:"请填写您的诉求", icon:"error"})
      return
    }

    wx.showLoading()
    console.log(this.data);
    wx.request({
      url: api.postReport(),
      method: "POST",
      data: this.data,
      fail(res) {
        wx.hideLoading()
        console.error(res)
        wx.showToast({ title: "提交失败", icon: "error" })
      },
      success(res) {
        console.log(res);
        wx.hideLoading()
        if (res.data.code == 1) {
          wx.showToast({title:"上报成功"})
          setTimeout(() => {
            wx.navigateBack()
          }, 1200);
        }else{
          wx.showToast({title:res.data.msg, icon:"error"})
        }
      }
    })
  }
})
//  生成报告ID
function genReportID() {
  let date: string = Date.now().toString().slice(3)
  let random: string = Math.ceil(Math.random() * 1000).toString()
  return date + random
}

//  获取手机号, 姓名, 和管理员的UID
function getPrivacyInfo(that: object) {
  wx.request({
    url: api.getUserPrivacyInfo(),
    method: "GET",
    header: {
      "content-type": "application/x-www-form-urlencoded"
    },
    data: {
      uid: app.globalData.userInfo.uid,
      token: app.globalData.userInfo.token,
    },
    success(res) {
      let data = res.data
      if (data.code == 1) {
        that.setData({
          name: data.data.name,
          phoneNum: data.data.phoneNum,
          adminUid: data.data.adminUid
        })
      }
    }
  })
}
// pages/mymessage1/mymessage1.js
var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    copyright: 'Copyright © 2021-2031 Lyuiot.All Rights Reserved.',
    motto: '开启您的健康之旅吧',
    devNum: 0,
    userImg: null,
    userNickName: null
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.setData({
      devNum: app.globalData.userInfo.deviceNum,
      userImg: app.globalData.userAvatarUrl,
      userNickName: app.globalData.userNickName
    })
  },

  goToAbout(){
    wx.navigateTo({url:"../logs/logs"})
  },

  goToReport(){
    wx.navigateTo({url:"../postReport/postReport?type=0"})
  },

  gotoBuy(){
    wx.navigateTo({url:"../postReport/postReport?type=1"})
  },

  goToFeeBack(){
    wx.navigateTo({url:"../postReport/postReport?type=2"})
  },

  repairSystem(){
    wx.showLoading()
    wx.clearStorageSync()
    this.loginOut()
    wx.hideLoading()
  },

  //  登出
  loginOut() {
    wx.reLaunch({
      url: "../login/login",
      fail(res) {
        console.log(res)
      }
    }
    )
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    if (wx.getUserProfile()) {
      this.setData({
        canIUseGetUserProfile: true
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})
// pages/TransitionPage/TransitionPage.ts
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    loginType: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    this.setData({
      loginType: app.globalData.userInfo.loginType == '1' ? true : false
    })
    this.loadViewComponent = this.data.loginType ? this.selectComponent("#adminView") : this.selectComponent("#userView")
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    //  执行组件的数据刷新方法
    this.loadViewComponent.updateSourceData()
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    //  执行组件的数据刷新方法
    this.loadViewComponent.updateSourceData()
  },
})
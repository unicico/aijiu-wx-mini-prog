var api = require("../../utils/getApiUrl")
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userName: "",
    phoneNum: "",
    adminUid:"10000"
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  userReg() {
    const that = this
    wx.showLoading();
    wx.login({
      success(res) {
        wx.hideLoading()
        if (res.code) {
          console.log(res.code)
          wx.request({
            url:api.userReg(),
            method:"POST",
            header:{
              "content-type" : "application/json"
            },
            dataType:"json",
            data: {
              code: res.code,
              name: that.data.userName,
              phoneNum:that.data.phoneNum,
              adminUid:that.data.adminUid
            },
            success(res){
              if(res.data.code && res.data.code == 1){
                //  更新全局变量
                app.globalData.userInfo.uid = res.data.data.uid
                app.globalData.userInfo.token = res.data.data.token
                //  跳转页面主页面
                wx.showToast({
                  title:"注册成功",
                  success(){
                    setTimeout(() => {
                      wx.reLaunch({
                        url:"../index/index"})
                    }, 1500);
                  }
                })
              }else{
                wx.showToast({
                  title: "注册失败",
                  icon:"error"
                })
              }
            },
            fail(res){
              wx.showToast({
                title: "请求失败",
                icon:"error"
              })
            }
          })
        } else {
          wx.showToast({
            title: "请求失败",
            icon:"error"
          })
        }
      },
      fail(res) {
        wx.hideLoading()
        wx.showToast({
          title: "请求失败",
          icon:"error"
        })
      }
    })
  }
})
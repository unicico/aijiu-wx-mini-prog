var api = require("../../utils/getApiUrl")
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userName: "",
    passWord: "",
    animationData: {},
    ani: {}
  },

  //事件处理函数
  bindViewTap: function () {
  },

  bindani: function () {
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },

  bindanimationend(){

  },


  // 给按钮绑定一个点击事件
  userReg() {
    wx.showLoading()
    const that = this
    wx.request({
      url: api.adminLoginUrl(),///跳到登录界面
      method: "POST",
      header: {
        "content-type": "application/json"
      },
      dataType: "json",
      data: {
        userName: that.data.userName,
        passWord: that.data.passWord,
      },
      success(res) {
        setTimeout(() => {
          wx.hideLoading()
        }, 0);
        
        //验证标识码
        if (res.data.code && res.data.code == 1) {
          //  更新全局变量
          app.globalData.userInfo.uid = 10000
          app.globalData.userInfo.token = 'vfOXcZuGJh6EASjiAmw/cA==AA'
          app.globalData.userInfo.loginType = 1

          //小球--创建动画
          var animation = wx.createAnimation({ //调用函数，返回animation
            duration: 500, //持续时间
            timingFunction: 'ease', //播放方式
            // delay:1000,  //延迟,
          });

          //执行动画的动作  
          animation.scale(5.3).opacity(0.9).step();  //扩大10倍
          
          //赋值新数据给animationData，开始动画
          that.setData({
            animationData: animation.export()
          });

          setTimeout(() => {
            wx.reLaunch({
              url: "../index/index"
            })
          }, 500);

        } else {
          wx.hideLoading()
          wx.showToast({
            title: res.data.msg,
            icon: "error"
          })
        }
      },
      fail(){
        wx.hideLoading()
        wx.showToast({
          title: "请求失败",
          icon:"error"
        })
      }
    })

    //按钮--创建动画
    var animation = wx.createAnimation({ //调用函数，返回animation
      duration: 0.1, //持续时间
      timingFunction: 'ease', //播放方式
    });

    //执行动画的动作  
    animation.scale(0.9).opacity(0.8).step();  //缩小并颜色变动
    animation.scale(1).opacity(1).step();  //缩小并颜色变动

    //赋值新数据给animationData，开始动画
    this.setData({
      ani: animation.export(),
    });
  }
})
// utils

function formatTime(date: Date) {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return (
    [year, month, day].map(formatNumber).join('/') +
    ' ' +
    [hour, minute, second].map(formatNumber).join(':')
  )
}

const formatNumber = (n: number) => {
  const s = n.toString()
  return s[1] ? s : '0' + s
}

function showError(title: string) {
  wx.hideLoading()
  wx.showToast({
    title: title ? title : "操作失败",
    icon: "error"
  })
}

function showSuccess(title: string) {
  wx.hideLoading()
  wx.showToast({
    title: title ? title : "操作成功",
  })
}

function getShowOnline(online: number) {
  if (online == null) {
    return "未知"
  }
  return online === 0 ? '离线' : '在线';
}

function getShowState(state: number) {
  switch (state) {
    case 0:
      return '空闲状态'
    case 1:
      return '加热状态'
    case 2:
      return '点火状态'
    case 3:
      return '散热状态'
    default:
      return '未知状态'
  }
}

function getShowColor(online: number, state: number) {
  if (online === 0) {
    return "#515151"
  }

  switch (state) {
    case 0:
      return '#00A870'
    case 1:
      return '#0052D9'
    case 2:
      return '#D43030'
    case 3:
        return '#77C4E0'
    default:
      return 'black'
  }
}

module.exports = {
  formatTime,
  showError,
  showSuccess,
  getShowOnline,
  getShowState,
  getShowColor
}
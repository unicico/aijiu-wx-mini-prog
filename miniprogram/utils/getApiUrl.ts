var app = getApp();

// const serverUrl = 'http://127.0.0.1:51888/'
// const serverUrl = 'https://www.unicico.cn/aijiuapi/'
const serverUrl = 'https://www.lyuiot.top/aijiuapi/'
// const serverUrl = 'https://www.lyuiot.top:51888'



function setMaxTemp(){
  return `${serverUrl}setMaxTemp`
}

function setMinTemp(){
  return `${serverUrl}setMinTemp`
}

function getUid() {
  return app.globalData.userInfo.uid
}

function getToken() {
  return app.globalData.userInfo.token
}

function adminLoginUrl() {
  return `${serverUrl}adminLogin`
}

function userLoginUrl() {
  return `${serverUrl}userLogin`
}

function userReg() {
  return `${serverUrl}userReg`
}

function getUserAllDevBasInfo() {
  return `${serverUrl}getUserAllDevBasInfo`
}

function devReg() {
  return `${serverUrl}devReg`
}

function getDevDetail() {
  return `${serverUrl}getDevDetail`
}

function getDevStateInfo() {
  return `${serverUrl}getDevStateInfo`
}
function setRelay() {
  return `${serverUrl}setRelay`
}

function startDev() {
  return `${serverUrl}startDev`
}
function startFire() {
  return `${serverUrl}startFire`
}
function stopDev() {
  return `${serverUrl}stopDev`
}
function getRelayInfo() {
  return `${serverUrl}getRelayInfo`
}

function getUserPrivacyInfo() {
  return `${serverUrl}getUserPrivacyInfo`
}

function adminGetAllUsers() {
  return `${serverUrl}adminGetAllUsers`
}

function adminGetAllDevices() {
  return `${serverUrl}adminGetAllDevices`
}

function adminUpdateDevInfo() {
  return `${serverUrl}adminUpdateDevInfo`
}

function adminDeleteDev() {
  return `${serverUrl}adminDeleteDev`
}

function adminDelUser() {
  return `${serverUrl}adminDelUser`
}

function adminUpdateUserInfo() {
  return `${serverUrl}adminUpdateUserInfo`
}

function postReport() {
  return `${serverUrl}postReport`
}

function adminGetReportList() {
  return `${serverUrl}adminGetReportList`
}

function adminGetReportDetail() {
  return `${serverUrl}adminGetReportDetail`
}

function adminSetReport() {
  return `${serverUrl}adminSetReport`
}

function adminIsHaveMessage() {
  return `${serverUrl}adminIsHaveMessage`
}

function stopFire() {
  return `${serverUrl}stopFire`
}

function userGetReportList() {
  return `${serverUrl}userGetReportList`
}

function userGetReportDetail() {
  return `${serverUrl}userGetReportDetail`
}

function userCancelPost() {
  return `${serverUrl}userCancelPost`
}

function userDeleteReport() {
  return `${serverUrl}userDeleteReport`
}

function devGoToCtlPage() {
  return `${serverUrl}devGoToCtlPage`
}

function deleteDev() {
  return `${serverUrl}deleteDev`
}

module.exports = {
  getToken,
  getUid,
  adminLoginUrl,
  userLoginUrl,
  userReg,
  getUserAllDevBasInfo,
  devReg,
  getDevDetail,
  getDevStateInfo,
  setRelay,
  startDev,
  startFire,
  stopDev,
  getRelayInfo,
  getUserPrivacyInfo,
  adminGetAllUsers,
  adminGetAllDevices,
  adminUpdateDevInfo,
  adminDeleteDev,
  adminDelUser,
  adminUpdateUserInfo,
  postReport,
  adminGetReportList,
  adminGetReportDetail,
  adminSetReport,
  adminIsHaveMessage,
  stopFire,
  userGetReportList,
  userGetReportDetail,
  userCancelPost,
  userDeleteReport,
  devGoToCtlPage,
  deleteDev,
  setMaxTemp,
  setMinTemp
}
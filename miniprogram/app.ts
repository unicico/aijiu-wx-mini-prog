// app.ts
App<IAppOption>({
  globalData: {
    userNickName: null,
    userAvatarUrl: null,
    userInfo: {
      loginType: 0,
      uid: "",
      token: "",
      deviceNum: 0,
    },
    selectedDev: {
      imie: "",
      name: "",
      online: "",
      tmp: "",
      addr: "",
      state: "",
    },
    debugging: {
      online: "",
      showColor: "",
      state: "",
      tmp: 0
    }
  },
  onLaunch() {
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        console.log(res.code)
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      },
    })
  },
})
创建 git 仓库:

```
mkdir aijiu-wx-mini-prog
cd aijiu-wx-mini-prog
git init 
touch README.md
git add README.md
git commit -m "first commit"
git remote add origin https://gitee.com/unicico/aijiu-wx-mini-prog.git
git push -u origin "master"
```

已有仓库?

```
cd existing_git_repo
git remote add origin https://gitee.com/unicico/aijiu-wx-mini-prog.git
git push -u origin "master"
```